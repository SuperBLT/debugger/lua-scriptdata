#include "ScriptData.h"

#include <sstream>
#include <algorithm>

#include <cmath>
#include <cassert>
#include <cinttypes>

#if defined(_WIN32)
#define LUA_EXPORT extern "C" __declspec(dllexport)
#else
#define LUA_EXPORT extern "C" __attribute__ ((visibility ("default")))
#endif

extern "C" {
#include "lua.h"
#include "lauxlib.h"
};

void write(lua_State *L, const SItem *item) {
	switch(item->GetId()) {
		case SNil::ID:
			lua_pushnil(L);
			break;
		case SBool::ID_T:
		case SBool::ID_F:
			lua_pushboolean(L, ((SBool*) item)->val);
			break;
		case SNum::ID:
			lua_pushnumber(L, ((SNum*) item)->val);
			break;
		case SString::ID:
			lua_pushstring(L, ((SString*) item)->val.c_str());
			break;
		case SVector::ID: {
			SVector *v = (SVector*) item;
			lua_newtable(L);
			lua_pushnumber(L, v->x);
			lua_setfield(L, -2, "x");
			lua_pushnumber(L, v->y);
			lua_setfield(L, -2, "y");
			lua_pushnumber(L, v->z);
			lua_setfield(L, -2, "z");

			lua_pushstring(L, "vector");
			lua_setfield(L, -2, "__meta_type");

			break;
		}
		case SQuaternion::ID: {
			SQuaternion *v = (SQuaternion*) item;
			lua_newtable(L);
			lua_pushnumber(L, v->x);
			lua_setfield(L, -2, "x");
			lua_pushnumber(L, v->y);
			lua_setfield(L, -2, "y");
			lua_pushnumber(L, v->z);
			lua_setfield(L, -2, "z");
			lua_pushnumber(L, v->w);
			lua_setfield(L, -2, "w");

			lua_pushstring(L, "quaternion");
			lua_setfield(L, -2, "__meta_type");

			break;
		}
		case SIdstring::ID: {
			lua_newtable(L);

			SIdstring *v = (SIdstring*) item;
			char hex[17];
			snprintf(hex, sizeof(hex), "%016" PRIx64, v->val);
			lua_pushstring(L, hex);
			lua_setfield(L, -2, "val");

			lua_pushstring(L, "idstring");
			lua_setfield(L, -2, "__meta_type");

			break;
		}
		case STable::ID: {
			STable *v = (STable*) item;
			lua_newtable(L);
			for(const auto &pair : v->items) {
				write(L, pair.first);
				write(L, pair.second);
				lua_settable(L, -3);
			}

			if(v->meta) {
				lua_pushstring(L, v->meta->val.c_str());
				lua_setfield(L, -2, "__meta_val");
			}

			lua_pushstring(L, "table");
			lua_setfield(L, -2, "__meta_type");

			break;
		}
		default:
			luaL_error(L, "Unknown item type %d", item->GetId());
	}
}

int ll_readsd(lua_State *L) {
	size_t len;
	const uint8_t *data = (const uint8_t*) luaL_checklstring(L, 1, &len);

	ScriptData sd(len, data);
	write(L, sd.GetRoot());

	return 1;
}

////

void write_xml_attribute(std::stringstream &out, lua_State *L, const SItem *item) {
	switch(item->GetId()) {
		case SNil::ID:
			out << "\"null\"";
			break;
		case SBool::ID_T:
			out << "\"true\"";
			break;
		case SBool::ID_F:
			out << "\"false\"";
			break;
		case SNum::ID:
			out << '"' << ((SNum*) item)->val << '"';
			break;
		case SString::ID:
			out << '"' << ((SString*) item)->val << '"';
			break;
		case SVector::ID: {
			SVector *v = (SVector*) item;
			out << '"' << v->x << " " << v->y << " " << v->z << '"';
			break;
		}
		case SQuaternion::ID: {
			SQuaternion *q = (SQuaternion*) item;
			out << '"' << q->x << " " << q->y << " " << q->z << " " << q->w << '"';
			break;
		}
		default:
			// TODO description here
			luaL_error(L, "Unknown XML attribute type %d", item->GetId());
	}
}

void write_xml_node(std::stringstream &out, lua_State *L, const SItem *item, int indents = 0) {
	switch(item->GetId()) {
		// TODO enable in a mode that doesn't match ScriptSerializer:to_custom_xml
		/*
		case SVector::ID: {
			SVector *v = (SVector*) item;
			out << "<Vector x=\"" << v->x << "\" y=\"" << v->y << "\" z=\"" << v->z << "\" />";
			return;
		}
		case SQuaternion::ID: {
			SQuaternion *v = (SQuaternion*) item;
			out << "<Quaternion x=\"" << v->x << "\" y=\"" << v->y << "\" z=\"" << v->z << "\" w=\"" << v->w << "\"/>";
			return;
		}
		*/
		case SIdstring::ID: {
			SIdstring *v = (SIdstring*) item;

			char hex[17];
			snprintf(hex, sizeof(hex), "%016" PRIx64, v->val);
			hex[16] = 0;

			out << "<Idstring w=\"" << hex << "\"/>";

			break;
		}
		case STable::ID: {
			STable *v = (STable*) item;

			std::map<const SItem*, const SItem*> attributes;
			std::vector<const SItem*> children;

			for(const auto &pair : v->items) {
				if(pair.first->GetId() == SNum::ID) {
					const SNum *sindex = (const SNum*) pair.first;
					int index = (int) round(sindex->val);

					// Lua indexes start at zero
					index -= 1;
					assert(index >= 0);

					if(children.size() <= index) {
						children.resize(index + 1);
					}
					children[index] = pair.second;
				} else {
					attributes[pair.first] = pair.second;
				}
			}

			std::string name = v->meta ? *v->meta : std::string("table");
			out << "<" << name; // TODO

			// temp
			// out << " id=" << v->index;

			std::string s_indent(indents, '\t');
			std::string s_indent_c(indents + 1, '\t');

			for(const auto &pair : attributes) {
				assert(pair.first->GetId() == SString::ID);
				const SString *id = (SString*) pair.first;

				if(pair.second->GetId() == STable::ID) {
					// Ensure the item is indeed in the children list
					std::vector<const SItem*>::const_iterator found = std::find(children.begin(), children.end(), pair.second);
					if(found == children.end()) {
						// TODO add warning that it isn't in the numerical index
						children.push_back(pair.second);
					}

					const STable *table = (const STable*) pair.second;

					if(!table->meta) {
						// Violate temp-ness for a second, and hope nothing breaks
						STable *mut = (STable*) table;
						mut->meta = (SString*) id;
					}

					continue;
				}

				out << " ";
				out << id->val;

				out << "=";
				write_xml_attribute(out, L, pair.second);
				//out << pair.second->index;
			}

			if(children.empty()) {
				out << " />";
			} else {
				out << ">\n";

				for(const SItem *child : children) {
					assert(child);
					out << s_indent_c;
					write_xml_node(out, L, child, indents + 1);
					out << "\n";
				}

				out << s_indent << "</" << name << ">";
			}

			break;
		}
		default: {
			//luaL_error(L, "Unknown item type %d", item->GetId());
			//luaL_error(L, "Unknown XML node type %d", item->GetId());
			out << "<value_node value=";
			write_xml_attribute(out, L, item);
			out << " />";
		}
	}
}

int ll_read_xml(lua_State *L) {
	size_t len;
	const uint8_t *data = (const uint8_t*) luaL_checklstring(L, 1, &len);

	std::stringstream str;

	ScriptData sd(len, data);
	write_xml_node(str, L, sd.GetRoot());

	std::string result = str.str();
	lua_pushstring(L, result.c_str());

	return 1;
}

////

LUA_EXPORT int luaopen_scriptdata(lua_State *L) {
	lua_newtable(L);

	lua_pushcfunction(L, ll_readsd);
	lua_setfield(L, -2, "read");

	lua_pushcfunction(L, ll_read_xml);
	lua_setfield(L, -2, "read_xml");

	return 1;
}

