
#include <cstring>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
int luaopen_scriptdata(lua_State *L);
}

const char *l = ""
"local fi = io.open(arg_fn, \"rb\");"
"local data = fi:read(\"*all\");"
"print(data:len());"
"local tbl = sd.read_xml(data);"
"print(tostring(tbl))"
"fi:close();"
;

int main(int argc, const char** argv) {
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);

	if(argc >= 2) {
		lua_pushstring(L, argv[1]);
	} else {
		lua_pushstring(L, "test.sd");
	}
	lua_setglobal(L, "arg_fn");

	luaopen_scriptdata(L);
	lua_setglobal(L, "sd");

	luaL_loadstring(L, l);
	int state = lua_pcall(L, 0, 0, 0);
	if(state)
		printf("Lua err: %s\n", lua_tostring(L, -1));

	lua_close(L);
}

