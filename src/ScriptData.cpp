#include "ScriptData.h"

#include <functional>
#include <cassert>

const SNil SNil::INSTANCE;
const SBool SBool::TRUE(true);
const SBool SBool::FALSE(false);

static bool is32bit;

template<typename Ptr>
struct RawVec {
	unsigned int count;
	unsigned int capacity;
	Ptr offset;
	Ptr ignore; // Allocator
};

template<typename Ptr>
struct RawStr {
	Ptr ignore; // Allocator
	Ptr str;
};

template<typename Ptr>
struct RawTable {
	Ptr meta;
	RawVec<Ptr> contents;
};

typedef RawVec<uint32_t> RawVec32;
typedef RawVec<uint64_t> RawVec64;
static_assert(sizeof(RawVec32) == 16, "RawVec (32-bit) is the wrong size!");
static_assert(sizeof(RawVec64) == 24, "RawVec (64-bit) is the wrong size!");

typedef RawStr<uint32_t> RawStr32;
typedef RawStr<uint64_t> RawStr64;
static_assert(sizeof(RawStr32) == 8, "RawStr (32-bit) is the wrong size!");
static_assert(sizeof(RawStr64) == 16, "RawStr (64-bit) is the wrong size!");

typedef RawTable<uint32_t> RawTable32;
typedef RawTable<uint64_t> RawTable64;
static_assert(sizeof(RawTable32) == 20, "RawTable (32-bit) is the wrong size!");
static_assert(sizeof(RawTable64) == 32, "RawTable (64-bit) is the wrong size!");

typedef uint32_t valid_t;

template<typename T>
struct VecInfo {
	size_t count;
	T *items;
};

template<typename T>
VecInfo<T> readVec(const uint8_t *data, size_t &offset) {
	VecInfo<T> info = {};

	if(is32bit) {
		RawVec32 &vec = *(RawVec32*) &data[offset];
		info.count = vec.count;
		info.items = (T*) &data[vec.offset];
		offset += sizeof(RawVec32);
	} else {
		RawVec64 &vec = *(RawVec64*) &data[offset];
		info.count = vec.count;
		info.items = (T*) &data[vec.offset];
		offset += sizeof(RawVec64);
	}

	return info;
}

template<typename from, typename to>
using Reader = std::function<void(const from&, to&)>;

template<typename from, typename to>
void readIntoVec(std::vector<to> &res, const uint8_t *data, size_t &offset, Reader<from, to> f) {
	VecInfo<from> info = readVec<from>(data, offset);

	res.clear();
	res.resize(info.count);

	for(size_t i=0; i<info.count; i++) {
		f(info.items[i], res[i]);
	}
}

SItem::~SItem() {
}

template<typename T>
void numberList(std::vector<T> &items) {
	for(size_t i=0; i<items.size(); i++) {
		items[i].index = i;
	}
}

void ScriptData::ReadTable(STable &out, std::pair<valid_t, valid_t> *data, size_t count, uint32_t meta) {
	for(size_t i=0; i<count; i++) {
		const SItem *key = Read(data[i].first);
		const SItem *val = Read(data[i].second);
		out.items[key] = val;
	}

	if(meta != ~0u) {
		out.meta = &strings[meta];
	} else {
		out.meta = nullptr;
	}
}

bool determine_is_32bit(size_t length, const uint8_t *data) {
	// The length of the 64-bit header
	// Any file shorter than this MUST be a 32-bit file
	const size_t header_len_64 = 8 + (6 * sizeof(RawVec64));

	if(length < header_len_64)
		return true;

	// See the format info file to see how this works

	// For the purposes of finding out the pointer width, treat the data
	//  as an integer array - the smaller units used in DslVector
	const uint32_t *ints = (const uint32_t*) data;

	// These macros check if the value at the given int position
#define CHECK_ZERO_32(index) if(ints[index] != 0) return false
#define CHECK_ZERO_64(index) if(ints[index] != 0) return true
#define CHECK_ZERO_64_PTR(index) if(ints[index] != 0 || ints[index+1] != 0) return true

	// Check the allocator slots are empty - if so, it's certainly not 64-bit
	CHECK_ZERO_64_PTR(0);
	CHECK_ZERO_64_PTR(6);
	CHECK_ZERO_64_PTR(12);
	CHECK_ZERO_64_PTR(18);
	CHECK_ZERO_64_PTR(24);
	CHECK_ZERO_64_PTR(30);
	CHECK_ZERO_64_PTR(36);

#undef CHECK_ZERO_32
#undef CHECK_ZERO_64
#undef CHECK_ZERO_64_PTR

	// At this point, we can be pretty sure it's a 64-bit file
	return false;
}

ScriptData::ScriptData(size_t length, const uint8_t *data) {
	is32bit = determine_is_32bit(length, data);

	size_t offset = 0;

	// Skip the ScriptData's allocator
	offset += is32bit ? 4 : 8;

	static_assert(sizeof(float) == 4, "incompatible float size");
	readIntoVec<float, SNum>(numbers, data, offset, [](const float &in, SNum &out) {
			out = SNum(in);
	});

	if(is32bit) {
		readIntoVec<RawStr32, SString>(strings, data, offset, [data](const RawStr32 &in, SString &out) {
				out = SString((const char*) &data[in.str]);
		});
	} else {
		readIntoVec<RawStr64, SString>(strings, data, offset, [data](const RawStr64 &in, SString &out) {
				out = SString((const char*) &data[in.str]);
		});
	}

	readIntoVec<float[3], SVector>(vectors, data, offset, [](const float (&in)[3], SVector &out) {
			out = SVector(in[0], in[1], in[2]);
	});

	readIntoVec<float[4], SQuaternion>(quats, data, offset, [](const float (&in)[4], SQuaternion &out) {
			out = SQuaternion(in[0], in[1], in[2], in[3]);
	});

	readIntoVec<uint64_t, SIdstring>(idstrings, data, offset, [](const uint64_t &in, SIdstring &out) {
			out = SIdstring(in);
	});

	if(is32bit) {
		readIntoVec<RawTable32, STable>(tables, data, offset, [data, this](const RawTable32 &in, STable &out) {
				out = STable();
				ReadTable(out, (std::pair<valid_t, valid_t>*) &data[in.contents.offset], in.contents.count, in.meta);
		});
	} else {
		readIntoVec<RawTable64, STable>(tables, data, offset, [data, this](const RawTable64 &in, STable &out) {
				out = STable();
				ReadTable(out, (std::pair<valid_t, valid_t>*) &data[in.contents.offset], in.contents.count, in.meta);
		});
	}

	numberList(numbers);
	numberList(strings);
	numberList(vectors);
	numberList(quats);
	numberList(idstrings);
	numberList(tables);

	uint32_t *val = (uint32_t*) &data[offset];
	root = Read(*val);
}

const SItem* ScriptData::Read(uint32_t val) {
	uint8_t type = val >> 24;
	uint32_t index = val & 0xFFFFFF;

	switch(type) {
		case SNil::ID: return &SNil::INSTANCE;
		case SBool::ID_F: return &SBool::FALSE;
		case SBool::ID_T: return &SBool::TRUE;
		case SNum::ID: return &numbers[index];
		case SString::ID: return &strings[index];
		case SVector::ID: return &vectors[index];
		case SQuaternion::ID: return &quats[index];
		case SIdstring::ID: return &idstrings[index];
		case STable::ID: return &tables[index];
		default:
			throw std::exception();
	}
}

